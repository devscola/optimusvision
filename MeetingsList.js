const editableListContainer = document.createElement('template')

editableListContainer.innerHTML = `
    <style>
      li, div > div {
        display: flex;
        align-items: center;
        justify-content: space-between;
      }
      li{
        border-bottom: 1px solid; 
      }

      .icon {
        background-color: #fff;
        border: none;
        cursor: pointer;
        float: right;
        font-size: 1.8rem;
      }
    </style>
    <h3>Event</h3>
    <ul class="item-list">
    </ul>
  `;

class MeetingsList extends HTMLElement {
  constructor() {
    super();

    this.root = this.attachShadow({ mode: 'open' })
    this.root.appendChild(editableListContainer.content.cloneNode(true))

    this.editableList = this.root.querySelector('.item-list')
    
    document.addEventListener('receivedMessage', this.showMeetings.bind(this))
    
    this.apiClient()
  }

  showMeetings(payload) {
    const messages = payload.detail[0]
    const meetings = messages.filter( message => message.includes('#evento'))
    console.log(messages)
    let result = `<li>`
    result += meetings.join(`</li><li>`)
    result += `</li>`
    this.editableList.innerHTML = result
  }

  apiClient() {
    var myHeardes = new Headers({
      'Authorization': 'Bearer e64fmwawnjgkid8xgkynbextqo',
    })
    var miInit = {
      method: 'GET',
      headers: myHeardes,
    };

    fetch('https://chat.devscola.org/api/v4/channels/4ffqiyqz9389z88axumq4tu7uy/posts', miInit)
      .then((response) => {
        return response.json();
      })
      .then((myJson) => {
        const messages = this.extractMessages(myJson)
        this.sendMessage('receivedMessage', messages)
      });
  }

  extractMessages(data){
    let orders = data.order
    const posts = data.posts
    let messages = []
    
    orders.forEach(order => {
      messages.push(posts[order].message)
    });

    return messages
  }

  sendMessage(topic, payload) {
    this.dispatchEvent(
      new CustomEvent(topic, {
        bubbles: true,
        detail: [payload],
        composed: true,
      })
    )
  }
}

customElements.define('meetings-list', MeetingsList);

