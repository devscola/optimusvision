const buttonTemplate = document.createElement('template')
buttonTemplate.innerHTML = `
    <button>+</button>
`

class Button extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open' })
        this.shadowDOM.appendChild(buttonTemplate.content.cloneNode(true))
        this.button = this.shadowDOM.querySelector('button')
    }

    connectedCallback() {
        this.button.addEventListener('click', () => {
            this.eventLaunch()
        })
    }

    eventLaunch(){

        var myHeardes = new Headers({
            'Authorization': 'Bearer e64fmwawnjgkid8xgkynbextqo',
        })

        var miInit = { method: 'GET',
                    headers: myHeardes,
        };

        fetch('https://chat.devscola.org/api/v4/channels/4ffqiyqz9389z88axumq4tu7uy/posts', miInit)
            .then((response) => {
                return response.json();
            })
            .then((myJson) => {
                let lastPost = myJson.order[0]
                let lastMessage = myJson.posts[lastPost].message
                console.log(lastMessage);
                document.querySelector('optimus-event').setAttribute("eventname", lastMessage)
     
            });

    }
}

window.customElements.define("optimus-button", Button)