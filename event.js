const eventTemplate = document.createElement('template')
eventTemplate.innerHTML = `
    <label>Hello</label>
`

class Event extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open' })
        this.shadowDOM.appendChild(eventTemplate.content.cloneNode(true))
        this.label = this.shadowDOM.querySelector('label')
    }
    
    static get observedAttributes() { 
        return ['eventname']
    }

    attributeChangedCallback(attribute, oldVal, newVal) {
        if (attribute === "eventname") {
            this.label.innerHTML = newVal
        }
    }

}

window.customElements.define("optimus-event", Event)